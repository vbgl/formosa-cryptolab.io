---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
---

The Formosa Crypto project federates multiple projects in machine-checked
cryptography and high-assurance cryptographic engineering under a single
banner, to better support developers and users.

Request an invitation by email (`join *at* formosa-crypto *dot* org`) to join
us on [Zulip](https://formosa-crypto.zulipchat.com).

{% if site.news != empty %}
[Formosa News]({% link news.markdown %})
====

{% for item in site.news %}
- [**{{ item.title }}**]({{ item.url | relative_url }}) ({{ item.date | date: '%B' }} {{ item.date | date : '%d' | plus:'0' }}{{ item.date | date : ', %Y' }})
{% endfor %}
{% endif %}

[Formosa Projects]({% link projects.markdown %})
====

{% for project in site.projects %}
- [**{{ project.project }}**]({{ project.url | relative_url }}) — [Project Website]({{ project.website }}) — [Project Repository]({{ project.git }})<br/>
  {{ project.short | markdownify }}
{% endfor %}

[Formosa People]({% link people.markdown %})
====

Formosa Funding
====

Formosa and its component projects are supported by a variety of funders. We
gratefully acknowledge their support, including historical support for the
founding projects.

{% for grant in site.funding %}
- **{{ grant.name }}** ({{ grant.date | date: '%b %Y'}}—{{ grant.end | date: '%b %Y'}}): {{ grant.funder }}, {{ grant.programme }}, {{ grant.number }}<br/>
  {{ grant.content | markdownify }}
{% endfor %}
